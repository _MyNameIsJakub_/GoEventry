module gitlab.com/_MyNameIsJakub_/GoEventry

go 1.14

require (
	cloud.google.com/go/bigquery v1.15.0
	google.golang.org/api v0.40.0
)
