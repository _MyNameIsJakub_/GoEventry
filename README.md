# Eventry

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/_MyNameIsJakub_/GoEventry.svg)](https://pkg.go.dev/gitlab.com/_MyNameIsJakub_/GoEventry)

Eventry is a Go library for logging end users' actions into BigQuery.

## Installation
Use the `go get` command to install Eventry properly.

```bash
$ go get gitlab.com/_MyNameIsJakub_/GoEventry
```

## Usage

First of all, you need to create initialize Eventry with your GCP credentials.

```go
eventry.Init(projectID, serviceAccountKey, datasetID)
```

`projectID` is your GCP project ID.

`serviceAccountKey` is a path to your GCP service account key file.

`datasetID` is your BigQuery dataset ID.

You can log any end user's action to BigQuery via Eventry after that.


```go
eventry.LogEvent(userID, eventName)
```

## Notice
Eventry is an experimental Go library. Don't use it in production, please.
