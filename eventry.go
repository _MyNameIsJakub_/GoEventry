package eventry

import (
	"cloud.google.com/go/bigquery"
	"context"
	"errors"
	"google.golang.org/api/option"
	"strings"
)

// UserID is an internal user identifier (IP address, username, e-mail address, ...).
// EventName is an internal event identifier. Use camelCase for names of your events, please.
type Event struct {
	UserID,
	EventName string
}

var config struct {
	projectID,
	serviceAccountKey,
	datasetID string
}

// Initialize a new Eventry instance with your GCP credentials.
func Init(projectID, serviceAccountKey, datasetID string) {
	config.projectID = projectID
	config.serviceAccountKey = serviceAccountKey
	config.datasetID = datasetID
}

// Log a new end user's action into BigQuery.
func LogEvent(userID, eventName string) error {
	ctx := context.Background()

	strings.TrimSpace(config.projectID)
	strings.TrimSpace(config.serviceAccountKey)
	strings.TrimSpace(config.datasetID)

	if config.projectID == "" {
		return errors.New("Failed to log this end user's event. Have you initialized Eventry with your GCP credentials?")
	} else if config.serviceAccountKey == "" {
		return errors.New("Failed to log this end user's event. Have you initialized Eventry with your GCP credentials?")
	} else if config.datasetID == "" {
		return errors.New("Failed to log this end user's event. Have you initialized Eventry with your GCP credentials?")
	}

	bigQuery, err := bigquery.NewClient(ctx, config.projectID, option.WithCredentialsFile(config.serviceAccountKey))
	if err != nil {
		return errors.New("Failed to create a BigQuery instance. Are you sure that your GCP credentials are correct?")
	}

	tableSchema, err := bigquery.InferSchema(Event{})
	if err != nil {
		return errors.New("Failed to log this end user's event. Check integrity of Eventry, please.")
	}

	_, err = bigQuery.Dataset(config.datasetID).Table("auditLog").Metadata(ctx)
	if err != nil {
		newTable := bigQuery.Dataset(config.datasetID).Table("auditLog")
		if err := newTable.Create(ctx,
			&bigquery.TableMetadata{
				Name:             eventName,
				Schema:           tableSchema,
				TimePartitioning: &bigquery.TimePartitioning{},
			}); err != nil {
			return errors.New("Failed to log this end user's event. Check integrity of Eventry, please.")
		}

		tableInserter := bigQuery.Dataset(config.datasetID).Table("auditLog").Inserter()

		userIDShadow := userID
		eventNameShadow := eventName

		data := []Event{
			{UserID: userIDShadow, EventName: eventNameShadow},
		}
		if err := tableInserter.Put(ctx, data); err != nil {
			return errors.New("Failed to log this end user's event. Check integrity of Eventry, please.")
		}

		return nil
	}

	tableInserter := bigQuery.Dataset(config.datasetID).Table("auditLog").Inserter()

	userIDShadow := userID
	eventNameShadow := eventName

	data := []Event{
		{UserID: userIDShadow, EventName: eventNameShadow},
	}
	if err := tableInserter.Put(ctx, data); err != nil {
		return errors.New("Failed to log this end user's event. Check integrity of Eventry, please.")
	}

	return nil
}
